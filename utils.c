/*
15/02/2020 Etienne M

Some utility functions to interface with the linux kernel
*/

#define _GNU_SOURCE /* See feature_test_macros(7) */

#include "utils.h"

#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

char* utils_JoinPathAndFile(const char* path,
                            size_t      path_len,
                            const char* file_name,
                            size_t      file_name_len,
                            size_t*     new_len)
{
    char*  directory = NULL;
    size_t dummy;

    if(new_len == NULL)
    {
        new_len = &dummy;
    }

    size_t directory_len = path_len + file_name_len + 1 + 1;

    // +1 for the '/' +1 for the \0
    if((directory = (char*)malloc(directory_len)) == NULL)
    {
        return NULL;
    }

    if(path[path_len - 1] == '/')
    {
        *new_len = sprintf(directory, "%s%s", path, file_name);
    }
    else
    {
        *new_len = sprintf(directory, "%s/%s", path, file_name);
    }
    return directory;
}

int utils_CopyStatsTo(struct stat* stat_buff, int to)
{
    struct timeval times[2];

    if(fchmod(to, stat_buff->st_mode) < 0)
    {
        PERROR("Could not chmod the destination file");
        return -1;
    }

    if(fchown(to, stat_buff->st_uid, stat_buff->st_gid) < 0)
    {
        PERROR("Could not chown the destination file");
        return -1;
    }

    times[0].tv_sec  = stat_buff->st_atim.tv_sec;
    times[0].tv_usec = stat_buff->st_atim.tv_nsec / 1000;

    times[1].tv_sec  = stat_buff->st_mtim.tv_sec;
    times[1].tv_usec = stat_buff->st_mtim.tv_nsec / 1000;

    // TIMESPEC_TO_TIMEVAL(&stat_buff->st_atim, &times[0]);
    // TIMESPEC_TO_TIMEVAL(&stat_buff->st_mtim, &times[1]);

    /*
     * As far as the creation time goes, most Linux file systems do not keep
     * track of this value. There is a ctime associated with files, but it
     * tracks when the file metadata was last changed. If the file never has its
     * permissions changed, it might happen to hold the creation time, but this
     * is a coincidence. Explicitly changing the file modification time counts
     * as a metadata change, so will also have the side effect of updating the
     * ctime.
     * https://askubuntu.com/questions/62492/how-can-i-change-the-date-modified-created-of-a-file
     */
    if(futimes(to, times) < 0)
    {
        PERROR("Could not utimes the destination file");
        return -1;
    }

    return to;
}

char* utils_BaseName(char* path)
{
    char* last;
    if((last = strrchr(path, '/')) == 0)
    {
        return path;
    }

    return last + 1;
}

ssize_t utils_WriteToFile(int fd, const char* buff, size_t n)
{
    ssize_t total_bytes_written = 0;

    while(n > 0)
    {
        ssize_t bytes_written;

        bytes_written = write(fd, buff + total_bytes_written, n);

        if(bytes_written < 0)
            return -1;

        total_bytes_written += bytes_written;
        n -= bytes_written;
    }
    return total_bytes_written;
}

int utils_CreateParentDir(const char* dir, mode_t mode)
{
    struct stat tmp;

    if(stat(dir, &tmp) < 0)
    {
        // destination file or directory does not exist
        if(errno == ENOENT)
        {
            char* tmp        = strdup(dir);
            char* parent_dir = dirname(tmp);
            int   ret        = CreateParentDir(parent_dir, mode);

            free(tmp);

            if(ret < 0)
            {
                return -1;
            }

            if(mkdir(dir, mode) < 0)
            {
                return -1;
            }
        }
    }
    else if((tmp.st_mode & S_IFMT) != S_IFDIR)
    {
        return -1;
    }

    return 0;
}

off_t utils_SizeOfFile(const char* path_name)
{
    struct stat stat_buf;

    if(lstat(path_name, &stat_buf) < 0)
    {
        return -1;
    }
    return stat_buf.st_size;
}

off_t utils_SizeOfFileOnDisk(const char* path_name)
{
    struct stat stat_buf;

    if(lstat(path_name, &stat_buf) < 0)
    {
        return -1;
    }
    return stat_buf.st_blocks * 512;
}

char* utils_ModeToString(mode_t mode)
{
    size_t str_len = 3 * 3 + 1;
    char*  str     = NULL;

    if((str = (char*)malloc(str_len + 1)) == NULL)
    {
        return NULL;
    }

    /* File types.  */
    // #define	__S_IFDIR	0040000	/* Directory.  */
    // #define	__S_IFCHR	0020000	/* Character device.  */
    // #define	__S_IFBLK	0060000	/* Block device.  */
    // #define	__S_IFREG	0100000	/* Regular file.  */
    // #define	__S_IFIFO	0010000	/* FIFO.  */
    // #define	__S_IFLNK	0120000	/* Symbolic link.  */
    // #define	__S_IFSOCK	0140000	/* Socket.  */

    // - : regular file.
    // d : directory.
    // c : character device file.
    // b : block device file.
    // s : local socket file.
    // p : named pipe.
    // l : symbolic link.
    switch(mode & S_IFMT)
    {
        case S_IFDIR:
            str[0] = 'd';
            break;
        case S_IFCHR:
            str[0] = 'c';
            break;
        case S_IFBLK:
            str[0] = 'b';
            break;
        case S_IFIFO:
            str[0] = 'p';
            break;
        case S_IFLNK:
            str[0] = 'l';
            break;
        case S_IFSOCK:
            str[0] = 's';
            break;
        case S_IFREG:
        default:
            str[0] = '-';
            break;
    }

    for(size_t i = 0; i < str_len - 1; i += 3)
    {
        str[i + 0 + 1] = (mode & (S_IRUSR >> i)) ? 'r' : '-';
        str[i + 1 + 1] = (mode & (S_IWUSR >> i)) ? 'w' : '-';
        str[i + 2 + 1] = (mode & (S_IXUSR >> i)) ? 'x' : '-';
    }

    str[str_len] = '\0';
    return str;
}

char* utils_SizeToStr(size_t size)
{
    size_t str_len = 4;
    char*  str     = NULL;

    if((str = (char*)malloc(str_len + 1)) == NULL)
    {
        return NULL;
    }

    char units[]  = " KMGTP";
    int  unit_idx = 0;

    float sizef = (float)size;

    for(; unit_idx < 5; ++unit_idx)
    {
        if((size /= 1024) == 0) // Ko not Kio
        {
            break;
        }
        sizef /= 1024;
    }

    //  21.3k ->  21k
    // 125.4K -> 125k
    if(sizef > 10)
    {
        snprintf(str, str_len + 1, "%3.f%c", sizef, units[unit_idx]);
    }
    else
    {
        if(unit_idx == 0)
        {
            snprintf(str, str_len + 1, "%4.f", sizef);
        }
        else
        {
            snprintf(str, str_len + 1, "%2.1f%c", sizef, units[unit_idx]);
        }
    }

    return str;
}

ssize_t utils_SizeOfDir(const char* path)
{
    DIR*           files = NULL;
    struct dirent* file  = NULL;

    size_t path_len = strlen(path);

    ssize_t size_of_dir = 0;

    if((files = opendir(path)) == NULL)
    {
        return -1;
    }

    while((file = readdir(files)))
    {
        char* path_name = JoinPathAndFile(path, path_len, file->d_name, strlen(file->d_name), NULL);

        ssize_t file_size = SizeOfFileOnDisk(path_name);
        if(file_size >= 0)
        {
            // printf("On: %s %ld\n", file->d_name, file_size);
            size_of_dir += file_size;
        }

        free(path_name);
    }

    closedir(files);
    return size_of_dir;
}

int utils_OpenRead(const char* filename)
{
    return open(filename, O_RDONLY | O_NDELAY);
}

int utils_OpenAppend(const char* filename)
{
    return open(filename, O_WRONLY | O_NDELAY | O_APPEND | O_CREAT, 0600);
}

int utils_OpenTrunc(const char* filename)
{
    return open(filename, O_WRONLY | O_NDELAY | O_TRUNC | O_CREAT, 0644);
}

int utils_OpenWrite(const char* filename)
{
    return open(filename, O_WRONLY | O_CREAT | O_NDELAY, 0644);
}

int utils_OpenReadWrite(const char* filename)
{
    return open(filename, O_RDWR | O_CREAT | O_NDELAY, 0644);
}

char* utils_MemMapShared(const char* filename, size_t* filesize)
{
    int   fd = OpenReadWrite(filename);
    char* map;

    if(fd >= 0)
    {
        register off_t o = lseek(fd, 0, SEEK_END);
        if(o == 0 || (sizeof(off_t) != sizeof(size_t) && o > (off_t)(size_t)-1))
        {
            close(fd);
            return NULL;
        }

        *filesize = (size_t)o;

        if(o)
        {
            map = mmap(0, *filesize, PROT_READ | PROT_WRITE, MAP_SHARED, fd, 0);
            if(map == (char*)-1)
            {
                map = NULL;
            }
        }
        else
        {
            map = NULL;
        }
        close(fd);
        return map;
    }
    return NULL;
}

char* utils_MemMapPrivate(const char* filename, size_t* filesize)
{
    int   fd = OpenRead(filename);
    char* map;
    if(fd >= 0)
    {
        register off_t o = lseek(fd, 0, SEEK_END);
        if(o == 0 || (sizeof(off_t) != sizeof(size_t) && o > (off_t)(size_t)-1))
        {
            close(fd);
            return NULL;
        }

        *filesize = (size_t)o;
        map       = (char*)mmap(0, *filesize, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);

        if(map == (char*)-1)
        {
            map = NULL;
        }

        close(fd);

        return map;
    }

    return NULL;
}

const char* utils_MemMapRead(const char* filename, size_t* filesize)
{
    int   fd = OpenRead(filename);
    char* map;

    if(fd >= 0)
    {
        register off_t o = lseek(fd, 0, SEEK_END);
        if(o == 0 || (sizeof(off_t) != sizeof(size_t) && o > (off_t)(size_t)-1))
        {
            close(fd);
            return NULL;
        }

        *filesize = (size_t)o;

        if(o > 0)
        {
            map = mmap(0, *filesize, PROT_READ, MAP_SHARED, fd, 0);
            if(map == (char*)-1)
            {
                map = NULL;
            }
        }
        else
        {
            map = NULL;
        }

        close(fd);
        return map;
    }
    return NULL;
}

int utils_MemUnMap(const char* mapped, size_t maplen)
{
    return maplen ? munmap((char*)mapped, maplen) : 0;
}

int utils_IsInvisibleChar(char c)
{
    return c == '\b' || c == '\f' || c == '\n' || c == '\r' || c == '\t' || c == '\v' || c == ' ';
}
