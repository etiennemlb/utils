/*
15/02/2020 Etienne M

Some utility functions to interface with the linux kernel
*/

#ifndef UTILS_UTILS_H
#define UTILS_UTILS_H

#include <sys/stat.h>
#include <sys/types.h>

#ifdef DEBUG_
#define PERROR(s) perror(s)
#else
#define PERROR(s) (void)(0)
#endif // DEBUG

/**
 * @brief Join a path to a filename adding a / if needed
 *
 *        path:  ./a/b/c
 *        filename: p
 *
 *        result: ./a/b/c/p
 *
 * @param path A cstring following the classic path format
 * @param path_len The len of path
 * @param file_name cstring following the classic path format
 * @param file_name_len The len of file_name
 * @param new_len A ptr to a size_t integer representing the new len of the path
 * @return char* The new path
 */
char* utils_JoinPathAndFile(const char* path,
                            size_t      path_len,
                            const char* file_name,
                            size_t      file_name_len,
                            size_t*     new_len);

/**
 * @brief Copy stats
 * 
 * @param stat_buff 
 * @param to Fd
 * @return int 0 if success -1 else, errno set appropriately
 */
int utils_CopyStatsTo(struct stat* stat_buff, int to);

/**
 * @brief Get the basename
 * 
 * @param path 
 * @return char* 
 */
char* utils_BaseName(char* path);

/**
 * @brief Useless
 * 
 * @param fd 
 * @param buff 
 * @param n 
 * @return ssize_t 
 */
ssize_t utils_WriteToFile(int fd, const char* buff, size_t n);

/**
 * @brief Create a directory recursively if necessary, similar to makedir -p
 *
 * @param dir The directory to create
 * @param mode The file rights to set
 * @return int zero if success < 0 else
 */
int utils_CreateParentDir(const char* dir, mode_t mode);

/**
 * @brief Returns the size of the file. Does not follow symbolic link.
 *
 * @param path_name pathname to
 * @return off_t size of file
 */
off_t utils_SizeOfFile(const char* path_name);

/**
 * @brief Returns the size of the file on disk, as block of 512 bytes used. Does
 * not follow symbolic link.
 *
 * @param path_name pathname to
 * @return off_t size of file on disk
 */
off_t utils_SizeOfFileOnDisk(const char* path_name);

/**
 * @brief Gives the représentation of the mode 'mode' as a cstring.
 *
 *
 * @param mode
 * @return char* The string representation of the mode Malloced str that can be
 *               freed.
 */
char* utils_ModeToString(mode_t mode);

/**
 * @brief Careful with signed types (e.g. ssize_t). Return Ko not Kio.
 *
 * @param size
 * @return char*
 */
char* utils_SizeToStr(size_t size);

/**
 * @brief No recursion, size of dir only.
 *
 * @param path
 * @return size_t
 */
ssize_t utils_SizeOfDir(const char* path);

/**
 * @brief Open a file for read
 *
 * @param filename
 * @return int
 */
int utils_OpenRead(const char* filename);

/**
 * @brief Open a file, will append
 *
 * @param filename
 * @return int
 */
int utils_OpenAppend(const char* filename);

/**
 * @brief Open a file, will truncate
 *
 * @param filename
 * @return int
 */
int utils_OpenTrunc(const char* filename);

/**
 * @brief Open a file for write
 *
 * @param filename
 * @return int
 */
int utils_OpenWrite(const char* filename);

int utils_OpenReadWrite(const char* filename);

/**
 * @brief Map a memory area to a file
 *
 * @param filename
 * @param filesize
 * @return char*
 */
char* utils_MemMapShared(const char* filename, size_t* filesize);

/**
 * @brief Map a memory area to a file
 *
 * @param filename
 * @param filesize
 * @return char*
 */
char* utils_MemMapPrivate(const char* filename, size_t* filesize);

/**
 * @brief Map a memory area to a file
 *
 * @param filename
 * @param filesize
 * @return const char*
 */
const char* utils_MemMapRead(const char* filename, size_t* filesize);

/**
 * @brief Unmap a maped memory area
 *
 * @param mapped
 * @param maplen mapped mem size
 * @return int < 0 if error
 */
int utils_MemUnMap(const char* mapped, size_t maplen);

/**
 * @brief http://crasseux.com/books/ctutorial/Special-characters.html
 *
 * @param c
 * @return int 1 if is invisible
 */
int utils_IsInvisibleChar(char c);

#endif //  UTILS_UTILS_H
